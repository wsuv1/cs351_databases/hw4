def WAHCompress(file, fileOut, run_size):
     # intialize variables
    runOfZero = 0
    runOfOne = 0
    bitMemory = ""
    currPos = 0
    totalRuns = 0
    totalLiterals = 0

    # loop through a column
    for x in range(0, 16):
        # on each line
        for line in file:
            # add it to the bitMemory
            bitMemory += str(line[x])
        while (currPos < len(bitMemory)):
            # run of zeroes
            if (bitMemory.count("0", currPos, currPos + run_size) == run_size):
                totalRuns += 1
                # add to run and figure out new encoding with new run count
                runOfZero += 1
                runs = format(runOfZero, 'b')
                # since this is a run of zeroes
                bitMemoryOld = "10"
                # we need the correct num of zeroes in the middle
                for i in range(1, run_size - len(runs)):
                    bitMemoryOld += "0"
                # add on run count
                bitMemoryOld += runs
                
            # run of ones
            elif (bitMemory.count("1", currPos, currPos + run_size) == run_size):
                totalRuns += 1
                # add to run and figure out new encoding with new run count
                runOfOne += 1
                runs = format(runOfOne, 'b')
                # since this is a run of ones
                bitMemoryOld = "11"
                # get correct number of zeroes in the middle
                for i in range(1, run_size - len(runs)):
                    bitMemoryOld += "0"
                # add on run count
                bitMemoryOld += runs

            # a literal
            else:
                totalLiterals += 1
                # we need to end the last run if there was one which means printing
                # it to the file
                if (runOfOne > 0):
                    fileOut.write(bitMemoryOld)
                    runOfOne = 0
                elif (runOfZero > 0):
                    fileOut.write(bitMemoryOld)
                    runOfZero = 0
                # here is where we pad a literal with zeroes if it's too small
                if (len(bitMemory[currPos : currPos + run_size]) != run_size):
                    for x in range(0, run_size - len(bitMemory[currPos : currPos + run_size])):
                        bitMemory += "0"
                # write the literal to the file
                fileOut.write("0" + bitMemory[currPos : currPos + run_size])
            # move our pointer one run to the right
            currPos += run_size
        fileOut.write("\n")
    print(fileOut.name)
    print("Fill Words: " + str(totalRuns) + "\nLiteral Words: " + str(totalLiterals) + "\n")