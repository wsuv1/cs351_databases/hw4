
import myQuicksort

def create_index(input_file, output_path, sorted):
    # if the path doesn't have a backslash, we give it one
    if (output_path == ""):
        output_path = "./"
    elif (output_path[len(output_path) - 1] != "/"): output_path += "/"
    if (sorted):
        # sort and then name the sorted bitmap file
        input_file_sorted = myQuicksort.sort(input_file, output_path)
        output_filename = input_file[input_file.find("/") + 1 : len(input_file)] + "_sorted"
    else:
        # set name for unsorted bitmap file
        output_filename = input_file[input_file.find("/") + 1 : len(input_file)]

    # Dictionary used to build bitmap
    bitmapDict = {
        "cat":     "1000",
        "dog":     "0100",
        "turtle":  "0010",
        "bird":    "0001",

        "1-10":    "1000000000",
        "11-20":   "0100000000",
        "21-30":   "0010000000",
        "31-40":   "0001000000",
        "41-50":   "0000100000",
        "51-60":   "0000010000",
        "61-70":   "0000001000",
        "71-80":   "0000000100",
        "81-90":   "0000000010",
        "91-100":  "0000000001",

        True:      "10",
        False:     "01"
    }

    # decide which file we need to open
    if (sorted):
        fileIn = open(input_file_sorted, "r")
    else:
        fileIn = open(input_file, "r")
    # create our bitmap file so we can write into it
    fileOut = open(output_path + output_filename, "w")

    file = fileIn.readlines()
    animal = ""
    age = ""
    adopted = ""

    for line in file:
        # keeps track of which column we're on
        attribute = 0
        for char in line:
            # comma seperates the data, so we use it to change columns
            if (char != ","):
                ## we add the character to the correct variable
                if (attribute == 0):
                    animal += char
                if (attribute == 1):
                    age += char
                if (attribute == 2):
                    # we only read the first character of the adopted attribute
                    # since there's no comma to stop us and that's all we need
                    if (char == "T"):
                        adopted = True
                        break
                    else:
                        adopted = False
                        break
            else:
                attribute += 1
        # here age bracket is assigned
        if   (int(age) <= 10):  age = "1-10"
        elif (int(age) <= 20):  age = "11-20"
        elif (int(age) <= 30):  age = "21-30"
        elif (int(age) <= 40):  age = "31-40"
        elif (int(age) <= 50):  age = "41-50"
        elif (int(age) <= 60):  age = "51-60"
        elif (int(age) <= 70):  age = "61-70"
        elif (int(age) <= 80):  age = "71-80"
        elif (int(age) <= 90):  age = "81-90"
        elif (int(age) <= 100): age = "91-100"

        # create a new string with the binary data from each category
        newStr = bitmapDict[animal] + bitmapDict[age] + bitmapDict[adopted]
        # write that string to our file
        fileOut.write(newStr + "\n")
        
        # reset the variables for another round
        animal = ""
        age = ""
    fileIn.close()
    fileOut.close()