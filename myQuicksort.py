
def sort(input_file, output_path):
    # open file to be sorted
    fileIn = open(input_file, "r")
    file = fileIn.readlines()
    # create array to hold entire file
    array = []
    array = [1 for l in open(input_file)]
    i = 0
    # put whole file in array
    for line in file:
        array[i] = line
        i += 1
    fileIn.close()
    # sort the file
    array = _quicksort(array, 0, len(array) - 1)
    # create the file
    fileOut = open(output_path + input_file[input_file.find("/") + 1 : input_file.find(".")] + "_sorted" + input_file[input_file.find(".") : len(input_file)], "w")
    # write the whole array to the new file
    i = 0
    for line in file:
        fileOut.write(array[i])
        i += 1
    fileOut.close()
    # return sorted file name and path
    return (output_path + input_file[input_file.find("/") + 1 : input_file.find(".")] + "_sorted" + input_file[input_file.find(".") : len(input_file)])

# Lomuto partition scheme quick sort. I referenced the quicksort wikipedia
# page, https://en.wikipedia.org/wiki/Quicksort. So credit goes to Tony Hoare
# and also Nico Lomuto.
def _quicksort(array, low, high):
    if (low < high):
        p = _partition(array, low, high)
        _quicksort(array, low, p - 1)
        _quicksort(array, p + 1, high)
    return array

def _partition(array, low, high):
    pivot = array[high]

    i = low - 1

    for j in range(low, high):
        if (array[j] <= pivot):
            i += 1
            array[i], array[j] = array[j], array[i]
    i += 1
    array[i], array[high] = array[high], array[i]
    return i
# End of quicksort algorithm.