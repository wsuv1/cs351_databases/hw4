import BBC, WAH

def compress_index(bitmap_index, output_path, compression_method, word_size):
    # if the output path has no backslash, we add one
    if (output_path[len(output_path) - 1] != "/"): output_path += "/"
    if (compression_method == "BBC"):
        word_size = 8
    # we set the name of our output file
    output_filename = bitmap_index[bitmap_index.find("/") + 1 :
        len(bitmap_index)] + "_" + compression_method + "_" + str(word_size)
    # set run length as one less than word size
    run_size = word_size - 1

    # set up files
    fileIn = open(bitmap_index, "r")
    fileOut = open(output_path + output_filename, "w")
    file = fileIn.readlines()

    if (compression_method == "WAH"):
        WAH.WAHCompress(file, fileOut, run_size)
    elif (compression_method == "BBC"):
        BBC.BBCCompress(file, fileOut)
    else:
        print("invalid compression method")
        return

    fileOut.close()
    fileIn.close()