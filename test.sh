#!/bin/bash

mkdir test/

RED='\033[0;31m'
GREEN='\033[0;32m'
GREY='\033[1;30m'
NC='\033[0m' # No Color

echo -e "Running main.py...${GREY}\n"
python main.py
echo -e "${NC}Finished running main.py\n"

echo "Byte Sizes: "
wc -c test/animals.txt
wc -c test/animals.txt_WAH_8
wc -c test/animals.txt_WAH_16
wc -c test/animals.txt_WAH_32
wc -c test/animals.txt_WAH_64
wc -c test/animals.txt_BBC_8
wc -c test/animals.txt_sorted_WAH_8
wc -c test/animals.txt_sorted_WAH_16
wc -c test/animals.txt_sorted_WAH_32
wc -c test/animals.txt_sorted_WAH_64
wc -c test/animals.txt_sorted_BBC_8


wc -c test/animals_small.txt
wc -c test/animals.txt_sorted
wc -c test/animals_small.txt_BBC_8
wc -c test/animals_small.txt_WAH_8
wc -c test/animals_small.txt_WAH_16
wc -c test/animals_small.txt_WAH_32
wc -c test/animals_small.txt_WAH_64
wc -c test/animals2.txt_BBC_8
wc -c test/animals2.txt_sorted_BBC_8
echo ""

echo -n "diff animals.txt "
if diff mine/bitmaps/animals test/animals.txt | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi
echo -n "diff animals.txt_sorted "
if diff mine/bitmaps/animalsSorted_sorted test/animals.txt_sorted | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi

echo -n "diff animals.txt_BBC_8 "
if diff mine/compressed/animals_BBC_8 test/animals.txt_BBC_8 | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi
echo -n "diff animals.txt_WAH_8 "
if diff mine/compressed/animals_WAH_8 test/animals.txt_WAH_8 | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi
echo -n "diff animals.txt_WAH_16 "
if diff mine/compressed/animals_WAH_16 test/animals.txt_WAH_16 | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi
echo -n "diff animals.txt_WAH_32 "
if diff mine/compressed/animals_WAH_32 test/animals.txt_WAH_32 | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi
echo -n "diff animals.txt_WAH_64 "
if diff mine/compressed/animals_WAH_64 test/animals.txt_WAH_64 | grep -q '.*'; then
    echo -e "${RED}failed${NC}"
else
    echo -e "${GREEN}passed${NC}"
fi

# rm -r test/
echo "Files cleaned."