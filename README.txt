In order to create bitmaps and compress them, all you need to import into
your test bed is compress.py and index.py. To run this code without an
external test bed, just run test.sh.

The sorted boolean in the create_index function will create a new sorted file
from an unsorted input_file automatically. The created sorted file will
have "_sorted" in it's file name and then the bitmap will be made from this
new file.

EX: if you insert "animals.txt" with sorted set to true then
"animals_sorted.txt" will be created, and the bitmap that will be created
will be named animals.txt_sorted.
