import index, compress

index.create_index("data/animals_small.txt", "test/", False)
compress.compress_index("test/animals_small.txt", "test/", "BBC", 8)

compress.compress_index("test/animals_small.txt", "test/", "WAH", 8)
compress.compress_index("test/animals_small.txt", "test/", "WAH", 16)
compress.compress_index("test/animals_small.txt", "test/", "WAH", 32)
compress.compress_index("test/animals_small.txt", "test/", "WAH", 64)

index.create_index("data/animals.txt", "test/", True)
index.create_index("data/animals.txt", "test/", False)
compress.compress_index("test/animals.txt", "test/", "BBC", 8)
compress.compress_index("test/animals.txt", "test/", "WAH", 8)
compress.compress_index("test/animals.txt", "test/", "WAH", 16)
compress.compress_index("test/animals.txt", "test/", "WAH", 32)
compress.compress_index("test/animals.txt", "test/", "WAH", 64)
compress.compress_index("test/animals.txt_sorted", "test/", "BBC", 8)
compress.compress_index("test/animals.txt_sorted", "test/", "WAH", 8)
compress.compress_index("test/animals.txt_sorted", "test/", "WAH", 16)
compress.compress_index("test/animals.txt_sorted", "test/", "WAH", 32)
compress.compress_index("test/animals.txt_sorted", "test/", "WAH", 64)

index.create_index("data2/animals2.txt", "test/", False)
index.create_index("data2/animals2.txt", "test/", True)

compress.compress_index("test/animals2.txt", "test/", "BBC", 8)
compress.compress_index("test/animals2.txt_sorted", "test/", "BBC", 8)