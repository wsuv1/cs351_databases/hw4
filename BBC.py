
# object that contains all meta data about a byte
class Byte:
    kind = ""
    begin = 0
    end = 0
    contents = ""
    index = 0

    def __init__(self, kind, begin, end, contents, index):
        self.kind = kind
        self.begin = begin
        self.end = end
        self.contents = contents
        self.index = index

# takes in bitmap and outputs compressed data to a new file
def BBCCompress(file, fileOut):
    # intialize variables
    run_size = 8
    currPos = 0
    bitMemory = ""
    index = 0
    totalRuns = 0
    totalLiterals = 0

    # loop through a column
    for column in range(0, 16):
        array = []
        index = 0
        # on each line
        for line in file:
            # add it to the bitMemory
            bitMemory += str(line[column])
        # loops through every byte in column
        while (currPos < len(bitMemory)):
            currByte = bitMemory[currPos : currPos + run_size]
            # run of zeroes
            if (currByte.count("0") == run_size):
                byte = Byte("run", currPos, currPos + run_size, currByte, index)
                index += 1
                array.append(byte)
            # a dirty byte
            elif (currByte.count("0") == run_size - 1):
                byte = Byte("dirty", currPos, currPos + run_size, currByte, index)
                index += 1
                array.append(byte)
            # a literal
            elif (currByte.count("0") < run_size - 1):
                byte = Byte("literal", currPos, currPos + run_size, currByte, index)
                index += 1
                array.append(byte)
            # moves pointer 1 byte over
            currPos += run_size
        # intialize variables for later
        literalBegin = 0
        literalEnd = 0
        runBegin = 0
        runEnd = 0
        runCount = ""
        literalCount = ""
        runOverflow = ""

        colRuns = 0
        colLiterals = 0

        for x in array:
            if (x.kind == "run"):
                colRuns += 1
            elif ((x.kind == "literal") | (x.kind == "dirty")):
                colLiterals += 1
        
        # loop through each element in our array of bytes
        for i in array:
            # if we're looking at a literal or dirty bit byte
            if ((i.kind == "literal") | (i.kind == "dirty")):
                # end the current run
                if ((runEnd - runBegin) > 0):
                    # get run count in binary
                    runCount = format(runEnd - runBegin, 'b')
                    # if we don't have overflow
                    if ((runEnd - runBegin) <= 6):
                        # extend the run count to be 3 bits, so it fits
                        for x in range(len(runCount), 3):
                            runCount = "0" + runCount
                        # set the header with the new run count
                        header = (runCount + "0" + literalCount)
                    # if the overflow only requires one byte
                    elif ((len(runCount) >= 3) & (len(runCount) <= 8)):
                        # extend the run count to use 1 byte
                        for x in range(len(runCount), 8):
                            runCount = "0" + runCount
                        # move it to the overflow variable
                        runOverflow = runCount
                        # set header to have "111" so we know there's overflow
                        header = ("111" + "0" + literalCount)
                        runCount = "111"
                    # if the overflow requires two bytes
                    elif (len(runCount) > 8):
                        # extend the run count to be 15 bits
                        for x in range(len(runCount), 15):
                            runCount = "0" + runCount
                        # set first bit to 1 so we know there's two overflow bytes
                        runCount = "1" + runCount
                        runOverflow = runCount
                        # set header to 111 so we know there's overflow
                        header = ("111" + "0" + literalCount)
                        runCount = "111"
                    # reset run count
                    runBegin = 0; runEnd = 0
                # checks if we're beginning a run off literals
                if ((literalEnd - literalBegin) == 0):
                    literalBegin = i.index
                    literalEnd = i.index
                # increments literal count
                literalEnd += 1
                # checks to see if we're out of room for literals
                if ((literalEnd - literalBegin) == 15):
                    # if no runs, we use 000 for header
                    if (runCount == ""):
                        runCount = "000"
                    
                    # create a header that shows we have 15 literals
                    header = (runCount + "01111")
                    fileOut.write(header)
                    # if there's overflow we need to write that too
                    if (runCount == "111"):
                                fileOut.write(runOverflow)
                    runCount = ""
                    # now we write the literals
                    for x in range(literalBegin, literalEnd):
                        fileOut.write(array[x].contents)
                    # reset literal count
                    literalBegin = 0; literalEnd = 0
            # if current byte is a run
            if (i.kind == "run"):
                # if a literal is ending
                if ((literalEnd - literalBegin) > 0):
                    # if it's a dirty bit literal and it's the only literal
                    if (((literalEnd - literalBegin) == 1) & (array[literalBegin].kind == "dirty")):
                            # if there's no run count we set it to 000
                            if (runCount == ""):
                                runCount = "000"
                            # set the dirt bit position in binary, and extend it to
                            # 4 bits
                            dirtyBitPos = format(array[literalBegin].contents.find("1"), 'b')
                            for x in range(len(dirtyBitPos), 4):
                                dirtyBitPos = "0" + dirtyBitPos
                            # create our header with the special bit set
                            # and dirty bit pos added
                            header = (runCount + "1" + dirtyBitPos)
                            # write it
                            fileOut.write(header)
                            # write the overflow if there is any
                            if (runCount == "111"):
                                fileOut.write(runOverflow)
                            runCount = ""
                            # reset literal count
                            literalBegin = 0; literalEnd = 0
                    # if it's a regular run of literals
                    else:
                        # encode literal count to binary
                        literalCount = format(literalEnd - literalBegin, 'b')
                        # extend it to 4 bits
                        for x in range(len(literalCount), 4):
                            literalCount = "0" + literalCount
                        # set run count to 000 if it's empty
                        if (runCount == ""):
                            runCount = "000"
                        # set header with run count and the literal count
                        header = (runCount + "0" + literalCount)
                        # write it
                        fileOut.write(header)
                        # write overflow if it's applicable
                        if (runCount == "111"):
                                    fileOut.write(runOverflow)
                        runCount = ""
                        # write the literals
                        for x in range(literalBegin, literalEnd):
                            fileOut.write(array[x].contents)
                        # end our count of literals
                        literalBegin = 0; literalEnd = 0
                # checks if we're starting a new run
                if ((runEnd - runBegin) == 0):
                    runBegin = i.index
                    runEnd = i.index
                # increment run count
                runEnd += 1
        # now we do almost everything again, with slight variation for the edge case
        # of the end of a column
        if ((literalEnd - literalBegin) > 0):
            if (((literalEnd - literalBegin) == 1) & (array[literalBegin].kind == "dirty")):
                if (runCount == ""):
                    runCount = "000"
                dirtyBitPos = format(array[literalBegin].contents.find("1"), 'b')
                for x in range(len(dirtyBitPos), 4):
                    dirtyBitPos = "0" + dirtyBitPos
                header = (runCount + "1" + dirtyBitPos)
                fileOut.write(header)
                if (runCount == "111"):
                    fileOut.write(runOverflow)
                runCount = ""

                literalBegin = 0; literalEnd = 0
            else:
                literalCount = format(literalEnd - literalBegin, 'b')

                for x in range(len(literalCount), 4):
                    literalCount = "0" + literalCount

                if (runCount == ""):
                    runCount = "000"

                header = (runCount + "0" + literalCount)
                fileOut.write(header)
                if (runCount == "111"):
                            fileOut.write(runOverflow)
                runCount = ""
                for x in range(literalBegin, literalEnd):
                    #print(array[x].contents)
                    fileOut.write(array[x].contents)

                literalBegin = 0; literalEnd = 0
        elif ((runEnd - runBegin) > 0):
            runCount = format(runEnd - runBegin, 'b')
            if ((runEnd - runBegin) <= 6):
                for x in range(len(runCount), 3):
                    runCount = "0" + runCount
                header = (runCount + "0" + "0000")
            elif ((len(runCount) >= 3) & (len(runCount) <= 8)):
                for x in range(len(runCount), 8):
                    runCount = "0" + runCount
                runOverflow = runCount
                header = ("111" + "0" + "0000")
                runCount = "111"
            elif (len(runCount) > 8):
                for x in range(len(runCount), 15):
                    runCount = "0" + runCount
                runCount = "1" + runCount
                runOverflow = runCount
                header = ("111" + "0" + "0000")
                runCount = "111"

            runBegin = 0; runEnd = 0
            fileOut.write(header)

            if (runCount == "111"):
                fileOut.write(runOverflow)
            runCount = ""
        # newline at end of column
        fileOut.write("\n")
        totalRuns += colRuns
        totalLiterals += colLiterals
    print(fileOut.name)
    print("Fill Words: " + str(totalRuns) + "\nLiteral Words: " + str(totalLiterals) + "\n")
